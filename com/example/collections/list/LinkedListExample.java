package com.example.collections.list;

import java.util.LinkedList;

import com.example.collections.CollectionsExampleAbstract;

public class LinkedListExample extends CollectionsExampleAbstract {

	LinkedList<String> linkedList;

	public LinkedListExample() {

		this.linkedList = new LinkedList<>();
	}

	@Override
	public void showExampleResult() {

		System.out.println("**** LinkedList Example *****");

		System.out.println("Add example");
		System.out.println("before add");
		System.out.println(linkedList.toString());

		System.out.println("After add");
		add();
		System.out.println(linkedList.toString());

		System.out.println("get example");
		get();

		System.out.println("Remove example");
		System.out.println("before remove");
		System.out.println(linkedList.toString());
		remove();

		System.out.println("after remove");
		System.out.println(linkedList.toString());

		System.out.println("size example");
		System.out.println(size());

	}

	@Override
	protected void add() {

		linkedList.add("Alma");
		linkedList.addFirst("R�pa");
		linkedList.addLast("Retek");

	}

	@Override
	protected void get() {
		System.out.println("Get first: " + linkedList.getFirst());
		System.out.println("Get last: " + linkedList.getLast());
		System.out.println("Get 0: " + linkedList.get(0));

	}

	@Override
	protected void remove() {
		linkedList.removeFirst();
		// linkedList.removeLast();
		// linkedList.remove(0);

	}

	@Override
	protected void set() {
		System.out.println("Set index 1 to Paradicsom");
		linkedList.set(1, "Paradicsom");

	}

	@Override
	protected int size() {
		return linkedList.size();
	}

}
