package com.example.collections.list;

import java.util.ArrayList;

import com.example.collections.CollectionsExampleAbstract;

public class ArrayListExample extends CollectionsExampleAbstract {

	/**
	 * ArrayList is a resizable-array implementation of the List interface. It
	 * implements all optional list operations, and permits all elements,
	 * including null. In addition to implementing the List interface, this
	 * class provides methods to manipulate the size of the array that is used
	 * internally to store the list.
	 */
	ArrayList<String> list;

	public ArrayListExample() {
		this.list = new ArrayList<>();
	}

	@Override
	protected void add() {

		list.add("Alma");
		list.add("K�rte");
		list.add("Alma");
		list.add("Alma");
		list.add(2, "Alma");

	}

	@Override
	protected void get() {

		System.out.println("list.get(0)" + list.get(0));
		// NPE
		// System.out.println("list.get(100)" + list.get(100));

	}

	@Override
	protected void remove() {

		list.remove(0);
		// search for first appearance of Alma
		list.remove("Alma");

	}

	@Override
	public void showExampleResult() {

		System.out.println("**** ArrayList ****");

		addExampleResult();
		getExampleResult();
		removeExampleResult();
		setExampleResult();
		sizeExampleResult();

		System.out.println("**** ArrayList END *****");

	}

	private void addExampleResult() {
		System.out.println(".add example");
		System.out.println("Before .add");

		System.out.println(list.toString());

		System.out.println("After .add");

		add();
		System.out.println(list.toString());

	}

	private void getExampleResult() {
		System.out.println("get example");

		get();
	}

	private void removeExampleResult() {
		System.out.println("remove example");
		System.out.println("Before remove");
		System.out.println(list.toString());

		remove();

		System.out.println("after remove");
		System.out.println(list.toString());

	}

	private void setExampleResult() {
		System.out.println("set example");
		System.out.println("Before set");

		System.out.println(list.toString());
		set();

		System.out.println("After set");
		System.out.println(list.toString());

	}

	private void sizeExampleResult() {
		System.out.println("size example");
		size();
	}

	@Override
	protected void set() {
		System.out.println("Set index 0 to R�pa");
		list.set(0, "R�pa");

	}

	@Override
	protected int size() {

		return list.size();

	}

}
