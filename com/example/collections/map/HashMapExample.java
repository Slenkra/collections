package com.example.collections.map;

import java.util.HashMap;

import com.example.collections.CollectionsExampleAbstract;

public class HashMapExample extends CollectionsExampleAbstract {

	HashMap<Integer, String> hashMap;

	public HashMapExample() {

		this.hashMap = new HashMap<>();

	}

	@Override
	public void showExampleResult() {

		System.out.println("**** HashMap example *****");
		System.out.println("No duplicates keys, no ordering");
		System.out.println("Add example");
		System.out.println("Before add");

		System.out.println(hashMap.toString());
		add();

		System.out.println("After add");
		System.out.println(hashMap.toString());

		System.out.println("get example");
		get();

		System.out.println("Remove example");
		System.out.println("Remove of element for key 1 ");
		remove();
		System.out.println(hashMap.toString());

		System.out.println("Set example");
		set();
		System.out.println(hashMap.toString());

		System.out.println("Size example");
		System.out.println(size());

	}

	@Override
	protected void add() {

		hashMap.put(1, "Asztal");
		hashMap.put(100, "T�ny�r");
		hashMap.put(2, "Sz�k");
		hashMap.put(56, "B�tor");
		hashMap.put(15, "Szekr�ny");
		// Duplicate value but unique key
		hashMap.put(78, "Asztal");

	}

	@Override
	protected void get() {

		System.out.println("Get for Key: 1 " + hashMap.get(1));
		System.out.println("Get for Key: 100 " + hashMap.get(100));
		System.out.println("Get for Key: 78 " + hashMap.get(78));

	}

	@Override
	protected void remove() {

		hashMap.remove(1);
	}

	@Override
	protected void set() {

		System.out.println("To modify value use replace(key, newValue)");
		System.out.println("Modify Element for key 100 to Szobabicikli");
		hashMap.replace(100, "Szobabicikli");

	}

	@Override
	protected int size() {
		return hashMap.size();
	}

}
