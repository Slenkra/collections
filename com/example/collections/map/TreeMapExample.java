package com.example.collections.map;

import java.util.TreeMap;

import com.example.collections.CollectionsExampleAbstract;

public class TreeMapExample extends CollectionsExampleAbstract {

	TreeMap<Integer, String> treeMap;

	public TreeMapExample() {

		this.treeMap = new TreeMap<>();

	}

	@Override
	public void showExampleResult() {

		System.out.println("**** treeMap example *****");
		System.out.println("No duplicates keys, has ordering");
		System.out.println("Add example");
		System.out.println("Before add");

		System.out.println(treeMap.toString());
		add();

		System.out.println("After add");
		System.out.println(treeMap.toString());

		System.out.println("get example");
		get();

		System.out.println("Remove example");
		System.out.println("Remove of element for key 1 ");
		remove();
		System.out.println(treeMap.toString());

		System.out.println("Set example");
		set();
		System.out.println(treeMap.toString());

		System.out.println("Size example");
		System.out.println(size());

	}

	@Override
	protected void add() {

		treeMap.put(1, "Asztal");
		treeMap.put(100, "T�ny�r");
		treeMap.put(2, "Sz�k");
		treeMap.put(56, "B�tor");
		treeMap.put(15, "Szekr�ny");
		treeMap.put(78, "Asztal");

	}

	@Override
	protected void get() {

		System.out.println("Get for Key: 1 " + treeMap.get(1));
		System.out.println("Get for Key: 100 " + treeMap.get(100));
		System.out.println("Get for Key: 78 " + treeMap.get(78));

	}

	@Override
	protected void remove() {

		treeMap.remove(1);
	}

	@Override
	protected void set() {

		System.out.println("To modify value use replace(key, newValue)");
		System.out.println("Set element for key 100 to Szobabicikli");
		treeMap.replace(100, "Szobabicikli");

	}

	@Override
	protected int size() {
		return treeMap.size();
	}
}
