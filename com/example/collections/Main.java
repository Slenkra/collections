package com.example.collections;

import com.example.collections.list.ArrayListExample;
import com.example.collections.list.LinkedListExample;
import com.example.collections.map.HashMapExample;
import com.example.collections.map.TreeMapExample;
import com.example.collections.set.HashSetExample;
import com.example.collections.set.TreeSetExample;

public class Main {

	public static void main(String[] args) {

		ArrayListExample arrayListExample = new ArrayListExample();
		arrayListExample.showExampleResult();

		LinkedListExample linkedListExample = new LinkedListExample();
		linkedListExample.showExampleResult();

		HashSetExample hashSetExample = new HashSetExample();
		hashSetExample.showExampleResult();

		TreeSetExample treeSetExample = new TreeSetExample();
		treeSetExample.showExampleResult();

		HashMapExample hashMapExample = new HashMapExample();
		hashMapExample.showExampleResult();

		TreeMapExample treeMapExample = new TreeMapExample();
		treeMapExample.showExampleResult();

	}

}
