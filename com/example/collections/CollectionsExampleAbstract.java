package com.example.collections;

public abstract class CollectionsExampleAbstract {

	public abstract void showExampleResult();

	protected abstract void add();

	protected abstract void get();

	protected abstract void remove();

	protected abstract void set();

	protected abstract int size();

}
