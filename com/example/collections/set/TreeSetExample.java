package com.example.collections.set;

import java.util.Iterator;
import java.util.TreeSet;

import com.example.collections.CollectionsExampleAbstract;

public class TreeSetExample extends CollectionsExampleAbstract {

	TreeSet<Integer> treeSet;

	public TreeSetExample() {

		this.treeSet = new TreeSet<>();

	}

	@Override
	public void showExampleResult() {

		System.out.println("**** TreeSet example *****");
		System.out.println("No duplicates, has ordering");
		System.out.println("Add example");
		System.out.println("Before add");
		System.out.println(treeSet.toString());
		add();

		System.out.println("After add - No duplicated elements");
		System.out.println(treeSet.toString());

		System.out.println("Remove example");
		System.out.println("Remove of element 100 ");
		remove();
		System.out.println(treeSet.toString());

		System.out.println("get example");
		get();

		System.out.println("Set example");
		set();

		System.out.println("Size example");
		System.out.println(size());

	}

	@Override
	protected void add() {

		System.out.println("Add 100");
		treeSet.add(100);
		System.out.println("Add 5000");
		treeSet.add(5000);
		System.out.println("Add 5");
		treeSet.add(5);
		System.out.println("Add -100");
		treeSet.add(-100);
		// Duplicate
		System.out.println("Add 100");
		treeSet.add(100);
		System.out.println("Add 100");
		treeSet.add(100);
		System.out.println("Add 100");
		treeSet.add(100);

	}

	@Override
	protected void get() {

		System.out.println("Using iterator");
		Iterator<Integer> it = treeSet.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
		}

		System.out.println("Without iterator");
		for (Integer element : treeSet) {
			System.out.print(" " + element + " ,");
		}

	}

	@Override
	protected void remove() {

		treeSet.remove(100);
		System.out.println("with clear() -> remove ALL elements");

	}

	@Override
	protected void set() {

		System.out.println("No method for set, cannot modify content");

	}

	@Override
	protected int size() {
		return treeSet.size();
	}

}
