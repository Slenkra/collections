package com.example.collections.set;

import java.util.HashSet;
import java.util.Iterator;

import com.example.collections.CollectionsExampleAbstract;

public class HashSetExample extends CollectionsExampleAbstract {

	HashSet<Integer> hashSetExample;

	public HashSetExample() {

		this.hashSetExample = new HashSet<Integer>();

	}

	@Override
	public void showExampleResult() {

		System.out.println("**** HashSet example *****");
		System.out.println("No duplicates, no ordering");
		System.out.println("Add example");
		System.out.println("Before add");
		System.out.println(hashSetExample.toString());
		add();

		System.out.println("After add - No duplicated elements");
		System.out.println(hashSetExample.toString());

		System.out.println("Remove example");
		System.out.println("Remove of element 100 ");
		remove();
		System.out.println(hashSetExample.toString());

		System.out.println("get example");
		get();

		System.out.println("Set example");
		set();

		System.out.println("Size example");
		System.out.println(size());

	}

	@Override
	protected void add() {

		System.out.println("Add 100");
		hashSetExample.add(100);
		System.out.println("Add 5000");
		hashSetExample.add(5000);
		System.out.println("Add 5");
		hashSetExample.add(5);
		System.out.println("Add -100");
		hashSetExample.add(-100);
		// Duplicate
		System.out.println("Add 100");
		hashSetExample.add(100);
		System.out.println("Add 100");
		hashSetExample.add(100);
		System.out.println("Add 100");
		hashSetExample.add(100);

	}

	@Override
	protected void get() {

		System.out.println("Using iterator");
		Iterator it = hashSetExample.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
		}

		System.out.println("without iterator");
		for (Integer element : hashSetExample) {
			System.out.print(" " + element + " ,");
		}

	}

	@Override
	protected void remove() {

		hashSetExample.remove(100);
		System.out.println("with clear() -> remove ALL elements");

	}

	@Override
	protected void set() {

		System.out.println("No method for set, cannot modify content");

	}

	@Override
	protected int size() {
		return hashSetExample.size();
	}

}
